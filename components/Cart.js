class Cart {
    
    
    create() {
        const cartData = JSON.parse(localStorage.getItem('cart')) || []
        const cartElement = document.createElement('div');
        cartElement.classList.add('cart')

        if(cartData.length === 0) {
            cartElement.innerHTML=`<h2 class="cart__empty">Корзина пуста:(</h2>`
            return cartElement
        }

        let li= ''
        cartData.forEach(({image, count, price, title, id}) => {
            li +=`<li class="cart__item">
                    <img src="${image}" alt="${title}">
                     <p>${title}</p>
                     <p>${count*price}BYN</p>
                     <button id="${id}" class="cart__delete__button">Удалить</button>
                  </li>`
            
        });

        cartElement.innerHTML = `<ul class="cart_items">
                                    ${li}
                                    </ul>`
        return cartElement;
    }
    
}

const cart = new Cart();
export default cart;