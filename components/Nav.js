import { getDataFromLocalStorage } from '../service/dataFromLocalStorageService.js'

class Nav {
    create() {
        const navElement = document.createElement('nav');
        const data = getDataFromLocalStorage()
        let li = ""

        data.forEach(({title, slug}) => {
            li += `<li class="nav__item"><a href="#${slug}">${title}</a></li>`
        })

        navElement.classList.add('nav');
        navElement.innerHTML = `<ul class = "nav__items">                                        
                                    ${li}
                                </ul>`
        return navElement;
    }
}

const nav = new Nav();

export default nav;