class Product {
    #productElement
    productData

    create(data) {
        this.#productElement = document.createElement('div');
        this.#productElement.classList.add('product');
        const { category, description, title, image, price, id } = data; 
        this.#productElement.innerHTML = `<div class="container">
                                            <div class="product__wrapper">
                                                <h1>${title}</h1>
                                                <img src="${image}">
                                                <h2>${category}</h2>
                                                <p>${description}</p>
                                                <span>${price}</span>
                                                <div class = "btns">
                                                <button class="go__back__btn">Назад</button>
                                                <button class="product__add__cart">Добавить в корзину</button> 
                                                </div>                                 
                                            </div>
                                          </div>`
        return this.#productElement
    }

    async getProductData(id) {
        const response = await fetch(`https://fakestoreapi.com/products/${id}`);
        this.productData = await response.json();
        return this.productData;
    }
    
    async init() {
        const id = location.hash.split('/')[1];
        const data = await this.getProductData(id)
        const productHTMLElement = this.create(data);        
        return productHTMLElement
    }
}

const product = new Product();
export default product;