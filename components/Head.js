const metaCharSet = document.createElement('meta');
metaCharSet.setAttribute('charset', 'UTF-8');
document.head.appendChild(metaCharSet);

const metaViewPort = document.createElement('meta');
metaViewPort.setAttribute('name', 'viewport-8');
metaViewPort.setAttribute('content', 'width=device-width, initial-scale=1.0');
document.head.appendChild(metaViewPort);

const title = document.createElement('title');
title.innerHTML = 'BuyHere';
document.head.appendChild(title);

const cssLink = document.createElement('link');
cssLink.setAttribute('rel', 'stylesheet');
cssLink.setAttribute('href', './css/style.css');
document.head.appendChild(cssLink);
