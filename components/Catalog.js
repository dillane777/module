import { CATALOG } from '../constants/constants.js'

class Catalog {
    #catalogElement
    catalogData

    create(data) {
        this.#catalogElement = document.createElement('div');
        this.#catalogElement.classList.add('catalog');

        let li="";
        data.forEach(({title, id, price, image}) => {
            li += `<li class="catalog__item">
                        <div class="catalog__item__image">
                            <img src="${image}" alt="${title}">
                        </div>
                        <div class="catalog__item__info">
                            <a class="catalog__item__title" href="#${CATALOG}/${id}" >${title}</a>
                            <div class="catalog__item__price">
                             ${price}
                            </div>
                            <button id="${id}" class="catalog__item__add__cart">Добавить в корзину</button>
                        </div>
                    </li>`
        });

        this.#catalogElement.innerHTML=`<ul class="catalog__items">
                                            ${li}
                                        </ul>`

        return this.#catalogElement
    }

    getCatalogData() {
        return fetch('https://fakestoreapi.com/products')
            .then(res=>res.json())
            .then(data => {
                this.catalogData = data
                return this.catalogData
            })
    }

   
    init() {
       const element =  this.getCatalogData().then((data) => this.create(data))
       return element
    }
}

const catalog = new Catalog;
export default catalog