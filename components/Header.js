import nav from './Nav.js';

class Header {
    create() {
        const header = document.createElement('header');
        const navHTMLElement = nav.create();
        header.classList.add('header');
        header.innerHTML = `<div class = "container">
                                <div class = "header__wrapper">
                                    <div class = "header__logo">
                                        <a href = "#">
                                            <img src= "https://image.neoseo.com.ua/image/catalog/foto_statti/topsovetovposozdanijubesplatnogologotipadljasajta/logo-design.png">
                                        </a>
                                    </div>
                                    ${navHTMLElement.outerHTML}
                                </div>
                            </div>`

        return header;
    }

}

const header = new Header();

export default header;